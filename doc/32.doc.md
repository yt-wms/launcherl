### 1. 简介
> 框架为您提供了自动生成的在线文档，可用于后端的API测试，也可以用于前端的联调。

### 2. 使用

在浏览器输入 [__http://localhost:8801/info/__](http://localhost:8801/info/) 可访问到API文档。您可以看到不同分组，以及分组下的方法。点击方法可以看到参数、返回值以及实体等信息。

在下面有个测试API，输入必要参数，即可对API进行测试。

### 3. 编写

在后端开发的时候需要对接口进行描述，以便于前端对接。之前用到的三个注解@HttpOpenApi、@HttpMethod 以及 @HttpParam 中都有description参数。这个参数将会参与到文档生成中。分别对应了对API分组的介绍、API的介绍、API参数的介绍。

光有这三个注解，无法对返回值对象，以及粗粒度接口的参数进行描述。对于实体的描述，使用@ApiEntity 与 @ApiField 两个注解进行描述。这两个注解可以分别注解到与属性上。

```java
@ApiEntity(description = "管理员实体")
public class AdminDTO extends SuperDTO implements PermissionOwner {
    /**
     * 管理员名
     */
    @ApiField(description = "管理员用户名")
    private String username;
```

### 4. 绑定枚举

我们的系统中枚举大部分是数字，需要与中国文字对应起来，但是你挨个个写，很累，而且耦合，改一个地方还要改第二个地方，所有就有了枚举绑定。

```java
@Data
@ApiEntity(description = "广告实体")
public class AdvertDO extends SuperDO {

    @ApiField(description = "广告类型", enums = AdvertType.class)
    private Integer type;

    /**
     * 广告关联类型
     */
    @ApiField(description = "广告关联类型", enums = AdvertUnionType.class)
    private Integer unionType;

```

使用 enums = 某个枚举类就行了

```java
public enum AdvertUnionType implements BaseEnums {
    PRODUCT(1, "产品"),
    CATEGORY(2, "类目"),
    KEYWORDS(3, "关键字"),
    PAGE(4, "页面"),;
```

注意这个枚举类必须要实现 BaseEnums 基类